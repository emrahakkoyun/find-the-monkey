package com.gojolo.findthemonkey;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class Start extends Activity {
     int screenHeight,screenWidth;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		ImageView maymun = (ImageView)findViewById(R.id.maymun);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth/5,screenHeight/6);
		maymun.setLayoutParams(params);
		maymun.setX((float)(screenWidth/2.50));
		maymun.setY(screenHeight/2);
		maymun.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sound();
				final Intent oyunsaha = new Intent(Start.this,Oyun.class);
				startActivity(oyunsaha);	
			}
		});
	}
	   public void sound()
	    {
	    	MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses14);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();
	    	    };
	    	});
	       
	    }
	    

}
