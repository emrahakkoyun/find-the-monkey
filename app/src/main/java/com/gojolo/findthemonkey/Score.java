package com.gojolo.findthemonkey;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Score extends Activity {
    int screenWidth,screenHeight;
    int score=0,eskiscore;
    TextView scoret;
    private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		Bundle extras = getIntent().getExtras();
	    score= extras.getInt("score");
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());	
		SharedPreferences.Editor editor = preferences.edit();
		eskiscore=preferences.getInt("score", 0);
		if(score>eskiscore)
		{
		editor.putInt("score",score);
		editor.commit();
		score=preferences.getInt("score", 0);
		}
		else{
			score=eskiscore;
		}
		 RelativeLayout.LayoutParams lp2 =new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		 scoret=new TextView(this);
		 scoret.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 50.f);
		 scoret.setTextColor(Color.GRAY);
		 scoret.setText(String.valueOf(score));
		 lp2.setMargins((int)(screenWidth/6),screenHeight/4, 0, 0);
		 scoret.setLayoutParams(lp2);
		 rlt.addView(scoret);
		
	}
	 public boolean onTouchEvent(MotionEvent event) {
		 Toast.makeText(getApplicationContext(), "X:"+String.valueOf(screenWidth/event.getX()) +"Y"+String.valueOf(screenHeight/event.getY()), Toast.LENGTH_LONG).show();
		 if(screenWidth/event.getX()<1.54&&screenWidth/event.getX()>1.04&&screenHeight/event.getY()<10.51&&screenHeight/event.getY()>1.42)
		 {
			 Intent nint = new Intent(Score.this,Oyun.class);
			 startActivity(nint);
			 finish();
		 }
		return true;

		}
	    @Override
	    public void onBackPressed() {
	       return;
	    }
}
