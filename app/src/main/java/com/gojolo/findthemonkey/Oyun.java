package com.gojolo.findthemonkey;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Oyun extends Activity {
    Integer[] animal ={R.drawable.h1,
	          R.drawable.h2,
	          R.drawable.h3,
	          R.drawable.h4,
	          R.drawable.h5,
	          R.drawable.h6,
	  		  R.drawable.h7,
	  		  R.drawable.h8,
	  		  R.drawable.h9,
	  		  R.drawable.h10,
	  		  R.drawable.h11,
	  		  R.drawable.h12,
	  		  R.drawable.h13,
	  		  R.drawable.h14,
	  	      R.drawable.hm
	          };
    int count=90;
    int yanlis;
    int dogru;
    boolean tb=true;
  private Handler hd;
    int screenWidth,screenHeight;
    int gecici,x=0;
    int[] box=new int[15];
    ImageView i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15;
    ImageView k1,k2,k3,k4,k5;
    TextView time,score;
	private InterstitialAd mInterstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_oyun);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		MobileAds.initialize(this,
				"ca-app-pub-1312048647642571~8915398244");
		hd = new Handler();
		yanlis=5;
		dogru=0;
		x=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		RelativeLayout.LayoutParams lp1 =new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		 time=new TextView(this);
		 time.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.f);
		 time.setTextColor(Color.BLACK);
		 lp1.setMargins((int)(screenWidth/2.18),screenHeight/23, 0, 0);
		 time.setLayoutParams(lp1);
		 rlt.addView(time);
		 RelativeLayout.LayoutParams lp2 =new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		 score=new TextView(this);
		 score.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.f);
		 score.setTextColor(Color.WHITE);
		 lp2.setMargins((int)(screenWidth/1.08),screenHeight/23, 0, 0);
		 score.setLayoutParams(lp2);
		 rlt.addView(score);
		 i1=new ImageView(this);
	     i2=new ImageView(this);
	     i3=new ImageView(this);
	     i4=new ImageView(this);
	     i5=new ImageView(this);
	     i6=new ImageView(this);
	     i7=new ImageView(this);
	     i8=new ImageView(this);
	     i9=new ImageView(this);
	     i10=new ImageView(this);
	     i11=new ImageView(this);
	     i12=new ImageView(this);
	     i13=new ImageView(this);
	     i14=new ImageView(this);
	     i15=new ImageView(this);
	     k1=new ImageView(this);
	     k2=new ImageView(this);
	     k3=new ImageView(this);
	     k4=new ImageView(this);
	     k5=new ImageView(this);
	    RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
	    RelativeLayout.LayoutParams kalp = new RelativeLayout.LayoutParams(screenWidth/18,screenHeight/16);
	    i1.setLayoutParams(p1);
	    i2.setLayoutParams(p1);
	    i3.setLayoutParams(p1);
	    i4.setLayoutParams(p1);
	    i5.setLayoutParams(p1);
	    i6.setLayoutParams(p1);
	    i7.setLayoutParams(p1);
	    i8.setLayoutParams(p1);
	    i9.setLayoutParams(p1);
	    i10.setLayoutParams(p1);
	    i11.setLayoutParams(p1);
	    i12.setLayoutParams(p1);
	    i13.setLayoutParams(p1);
	    i14.setLayoutParams(p1);
	    i15.setLayoutParams(p1);
	    k1.setLayoutParams(kalp);
	    k2.setLayoutParams(kalp);
	    k3.setLayoutParams(kalp);
	    k4.setLayoutParams(kalp);
	    k5.setLayoutParams(kalp);
	    rlt.addView(i1);
	    rlt.addView(i2);
	    rlt.addView(i3);
	    rlt.addView(i4);
	    rlt.addView(i5);
	    rlt.addView(i6);
	    rlt.addView(i7);
	    rlt.addView(i8);
	    rlt.addView(i9);
	    rlt.addView(i10);
	    rlt.addView(i11);
	    rlt.addView(i12);
	    rlt.addView(i13);
	    rlt.addView(i14);
	    rlt.addView(i15);
	    rlt.addView(k1);
	    rlt.addView(k2);
	    rlt.addView(k3);
	    rlt.addView(k4);
	    rlt.addView(k5);
	    i1.setBackgroundResource(animal[0]);
	    i2.setBackgroundResource(animal[1]);
	    i3.setBackgroundResource(animal[2]);
	    i4.setBackgroundResource(animal[3]);
	    i5.setBackgroundResource(animal[4]);
	    i6.setBackgroundResource(animal[5]);
	    i7.setBackgroundResource(animal[6]);
	    i8.setBackgroundResource(animal[7]);
	    i9.setBackgroundResource(animal[8]);
	    i10.setBackgroundResource(animal[9]);
	    i11.setBackgroundResource(animal[10]);
	    i12.setBackgroundResource(animal[11]);
	    i13.setBackgroundResource(animal[12]);
	    i14.setBackgroundResource(animal[13]);
	    i15.setBackgroundResource(animal[14]);
	    k1.setBackgroundResource(R.drawable.kalp);
	    k2.setBackgroundResource(R.drawable.kalp);
	    k3.setBackgroundResource(R.drawable.kalp);
	    k4.setBackgroundResource(R.drawable.kalp);
	    k5.setBackgroundResource(R.drawable.kalp);
		i1.setX((float)(screenWidth/8.85));
		i1.setY((float)(screenHeight/3.71));
		i2.setX((float)(screenWidth/3.62));
		i2.setY((float)(screenHeight/3.71));
		i3.setX((float)(screenWidth/2.23));
		i3.setY((float)(screenHeight/3.71));
		i4.setX((float)(screenWidth/1.59));
		i4.setY((float)(screenHeight/3.71));
		i5.setX((float)(screenWidth/1.25));
		i5.setY((float)(screenHeight/3.71));
		i6.setX((float)(screenWidth/8.85));
		i6.setY((float)(screenHeight/1.90));
		i7.setX((float)(screenWidth/3.62));
		i7.setY((float)(screenHeight/1.90));
		i8.setX((float)(screenWidth/2.23));
		i8.setY((float)(screenHeight/1.90));
		i9.setX((float)(screenWidth/1.59));
		i9.setY((float)(screenHeight/1.90));
		i10.setX((float)(screenWidth/1.25));
		i10.setY((float)(screenHeight/1.90));
		i11.setX((float)(screenWidth/8.85));
		i11.setY((float)(screenHeight/1.29));
		i12.setX((float)(screenWidth/3.62));
		i12.setY((float)(screenHeight/1.29));
		i13.setX((float)(screenWidth/2.23));
		i13.setY((float)(screenHeight/1.29));
		i14.setX((float)(screenWidth/1.59));
		i14.setY((float)(screenHeight/1.29));
		i15.setX((float)(screenWidth/1.25));
		i15.setY((float)(screenHeight/1.29));    
		k1.setX((float)(screenWidth/3.51));
		k1.setY((float)(screenHeight/13.70));
		k2.setX((float)(screenWidth/4.52));
		k2.setY((float)(screenHeight/13.70));
		k3.setX((float)(screenWidth/6.21));
		k3.setY((float)(screenHeight/13.70));
		k4.setX((float)(screenWidth/10.00));
		k4.setY((float)(screenHeight/13.70));
		k5.setX((float)(screenWidth/26.14));
		k5.setY((float)(screenHeight/13.70));
        Thread t = new Thread(new Runnable() {     
            
            public void run(){  
           
                  while (tb==true){
                 
                        try{
                              Thread.sleep(1000);                                                                                        
                        }
                        catch(InterruptedException e)
                        { }
                                         
                        hd.post(new Runnable(){                        
                                                     
                              public void run(){  
                            	  if(count>0)
                             	 { 
                            		  time.setText(String.valueOf(count));
                            			Random rt = new Random();
                            	       	gecici=rt.nextInt(15);
                            	       	box[0]=gecici;
                            	       	for(int l=1;l<15;l++)
                            	       	{   gecici=rt.nextInt(15);
                            	       		for(int k=0;k<l;k++)
                            	       		{
                            	       			if(box[k]==gecici)
                            	       			{
                            	       				l--;
                            	       				break;
                            	       			}
                            	       			else{
                            	       				box[l]=gecici;
                            	       			}
                            	       		}
                            	       	}	
                                       	i1.setBackgroundResource(animal[box[0]]);
                                       	i1.setTag(animal[box[0]]);
                                   	    i2.setBackgroundResource(animal[box[1]]);
                                   	    i2.setTag(animal[box[1]]);
                                   	    i3.setBackgroundResource(animal[box[2]]);
                                   	    i3.setTag(animal[box[2]]);
                                   	    i4.setBackgroundResource(animal[box[3]]);
                                   	    i4.setTag(animal[box[3]]);
                                   	    i5.setBackgroundResource(animal[box[4]]);
                                    	i5.setTag(animal[box[4]]);
                                   	    i6.setBackgroundResource(animal[box[5]]);
                                   	    i6.setTag(animal[box[5]]);
                                   	    i7.setBackgroundResource(animal[box[6]]);
                                   	    i7.setTag(animal[box[6]]);
                                   	    i8.setBackgroundResource(animal[box[7]]);
                                   	    i8.setTag(animal[box[7]]);
                                   	    i9.setBackgroundResource(animal[box[8]]);
                                   	    i9.setTag(animal[box[8]]);
                                   	    i10.setBackgroundResource(animal[box[9]]);
                                   	    i10.setTag(animal[box[9]]);
                                   	    i11.setBackgroundResource(animal[box[10]]);
                                   	    i11.setTag(animal[box[10]]);
                                   	    i12.setBackgroundResource(animal[box[11]]);
                                   	    i12.setTag(animal[box[11]]);
                                   	    i13.setBackgroundResource(animal[box[12]]);
                                   	    i13.setTag(animal[box[12]]);
                                   	    i14.setBackgroundResource(animal[box[13]]);
                                   	    i14.setTag(animal[box[13]]);
                                   	    i15.setBackgroundResource(animal[box[14]]);
                                   	    i15.setTag(animal[box[14]]);
                                   		if(i1.getTag()==animal[0] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[0] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[0] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[0] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[0] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[0] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[0] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[0] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[0] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[0] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[0] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[0] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[0] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[0] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[0] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(1);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                   		if(i1.getTag()==animal[1] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    											
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[1] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[1] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[1] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[1] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[1] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[1] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[1] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[1] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[1] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[1] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[1] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[1] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[1] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[1] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(2);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                   		if(i1.getTag()==animal[2] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    											
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[2] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[2] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[2] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[2] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[2] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[2] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[2] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[2] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[2] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[2] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[2] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[2] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[2] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[2] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(3);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                   		if(i1.getTag()==animal[3] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    											
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[3] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[3] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[3] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[3] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[3] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[3] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[3] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[3] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[3] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[3] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[3] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[3] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[3] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[3] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(4);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                   		if(i1.getTag()==animal[4] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    											
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[4] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[4] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[4] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[4] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[4] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[4] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[4] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[4] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[4] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[4] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[4] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[4] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[4] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[4] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(5);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                   		if(i1.getTag()==animal[5] )
                               		 {
                                      	 i1.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    											
    										}
    									}); 
                               		 }
                                      	if(i2.getTag()==animal[5] )
                              		 {
                                         i2.setOnClickListener(new View.OnClickListener() {
    										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i3.getTag()==animal[5] )
                              		 {
                                         i3.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i4.getTag()==animal[5] )
                           		 {
                                         i4.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 
                           		 }
                                      	if(i5.getTag()==animal[5] )
                              		 {
                                         i5.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i6.getTag()==animal[5] )
                              		 {
                                         i6.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i7.getTag()==animal[5] )
                              		 {
                                         i7.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 
                              		 }
                                      	if(i8.getTag()==animal[5] )
                              		 {
                                         i8.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i9.getTag()==animal[5] )
                              		 {
                                         i9.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i10.getTag()==animal[5] )
                              		 {
                                         i10.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i11.getTag()==animal[5] )
                              		 {
                                         i11.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i12.getTag()==animal[5] )
                              		 {
                                         i12.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i13.getTag()==animal[5] )
                              		 {
                                         i13.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i14.getTag()==animal[5] )
                              		 {
                                         i14.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                              		 }
                                      	if(i15.getTag()==animal[5] )
                              		 {
                                         i15.setOnClickListener(new View.OnClickListener() {
     										
    										@Override
    										public void onClick(View v) {
    											sound(6);
    											yanlis--;
    										}
    									}); 	
                                         
                               		 }
                                	if(i1.getTag()==animal[6] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[6] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[6] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[6] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[6] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[6] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[6] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[6] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[6] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[6] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[6] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[6] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[6] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[6] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(7);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[6] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(7);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[7] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[7] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[7] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[7] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[7] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[7] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[7] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[7] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[7] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[7] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[7] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[7] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[7] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[7] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(8);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[7] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(8);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[8] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[8] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[8] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[8] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[8] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[8] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[8] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[8] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[8] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[8] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[8] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[8] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[8] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[8] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(9);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[8] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(9);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[9] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[9] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[9] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[9] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[9] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[9] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[9] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[9] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[9] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[9] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[9] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[9] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[9] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[9] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(10);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[9] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(10);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[10] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[10] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[10] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[10] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[10] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[10] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[10] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[10] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[10] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[10] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[10] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[10] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[10] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[10] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(11);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[10] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(11);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[11] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[11] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[11] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[11] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[11] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[11] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[11] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[11] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[11] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[11] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[11] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[11] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[11] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[11] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(12);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[11] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(12);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[12] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[12] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[12] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[12] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[12] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[12] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[12] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[12] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[12] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[12] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[12] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[12] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[12] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[12] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(13);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[12] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                	if(i1.getTag()==animal[13] )
                           		 {
                                  	 i1.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
											
										}
									}); 
                           		 }
                                  	if(i2.getTag()==animal[13] )
                          		 {
                                     i2.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i3.getTag()==animal[13] )
                          		 {
                                     i3.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i4.getTag()==animal[13] )
                       		 {
                                     i4.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 
                       		 }
                                  	if(i5.getTag()==animal[13] )
                          		 {
                                     i5.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i6.getTag()==animal[13] )
                          		 {
                                     i6.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(13);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i7.getTag()==animal[13] )
                          		 {
                                     i7.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 
                          		 }
                                  	if(i8.getTag()==animal[13] )
                          		 {
                                     i8.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i9.getTag()==animal[13] )
                          		 {
                                     i9.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i10.getTag()==animal[13] )
                          		 {
                                     i10.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i11.getTag()==animal[13] )
                          		 {
                                     i11.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i12.getTag()==animal[13] )
                          		 {
                                     i12.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i13.getTag()==animal[13] )
                          		 {
                                     i13.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i14.getTag()==animal[13] )
                          		 {
                                     i14.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											
											sound(14);
											yanlis--;
										}
									}); 	
                          		 }
                                  	if(i15.getTag()==animal[13] )
                          		 {
                                     i15.setOnClickListener(new View.OnClickListener() {
 										
										@Override
										public void onClick(View v) {
											sound(14);
											yanlis--;
										}
									}); 	
                                     
                           		 }
                                   	if(i1.getTag()==animal[14] )
                            		 {
                                   	 i1.setOnClickListener(new View.OnClickListener() {
 										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 											
 										}
 									}); 
                            		 }
                                   	if(i2.getTag()==animal[14] )
                           		 {
                                      i2.setOnClickListener(new View.OnClickListener() {
 										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i3.getTag()==animal[14] )
                           		 {
                                      i3.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 
                           		 }
                                   	if(i4.getTag()==animal[14] )
                        		 {
                                      i4.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 
                        		 }
                                   	if(i5.getTag()==animal[14] )
                           		 {
                                      i5.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i6.getTag()==animal[14] )
                           		 {
                                      i6.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 
                           		 }
                                   	if(i7.getTag()==animal[14] )
                           		 {
                                      i7.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 
                           		 }
                                   	if(i8.getTag()==animal[14] )
                           		 {
                                      i8.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i9.getTag()==animal[14] )
                           		 {
                                      i9.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i10.getTag()==animal[14] )
                           		 {
                                      i10.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i11.getTag()==animal[14] )
                           		 {
                                      i11.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i12.getTag()==animal[14] )
                           		 {
                                      i12.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i13.getTag()==animal[14] )
                           		 {
                                      i13.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i14.getTag()==animal[14] )
                           		 {
                                      i14.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											
 											sound(15);
 											dogru++;
 										}
 									}); 	
                           		 }
                                   	if(i15.getTag()==animal[14] )
                           		 {
                                      i15.setOnClickListener(new View.OnClickListener() {
  										
 										@Override
 										public void onClick(View v) {
 											sound(15);
 											dogru++;
 										}
 									}); 	
                                      
                            		 }
                                   	  score.setText(String.valueOf(dogru));
                                   	  if(yanlis==4)
                                   	  {
                                   		  k1.setVisibility(View.GONE);
                                   	  }
                                   	if(yanlis==3)
                                 	  {
                                       	 k1.setVisibility(View.GONE);
                                 		  k2.setVisibility(View.GONE);
                                 	  }
                                   	if(yanlis==2)
                                 	  {
                                   		k1.setVisibility(View.GONE);
                               		  k2.setVisibility(View.GONE);
                                 		  k3.setVisibility(View.GONE);
                                 	  }
                                   	if(yanlis==1)
                                 	  {
                                   		k1.setVisibility(View.GONE);
                                 		  k2.setVisibility(View.GONE);
                                   		  k3.setVisibility(View.GONE);
                                 		  k4.setVisibility(View.GONE);
                                 	  }
                                   	if(yanlis<=0)
                                 	  {
                                   		k1.setVisibility(View.GONE);
                               		  k2.setVisibility(View.GONE);
                                 		  k3.setVisibility(View.GONE);
                               		  k4.setVisibility(View.GONE);
                                 		  k5.setVisibility(View.GONE);
                                 		  tb=false;
                                 	  }

                                	 count--;
                             	 }
                            	  else{
                            		  tb=false;
                            		 
                            	  }
                              }
                        });
                         if(tb==false)
                         {
                        	 if(x==0)
                        	 { finish();
                        		 inrequestadd(dogru);}
                        	 x++;
                              break;
                         }
                  } // while
            }
        });
        t.start();
	}
	public void sound(int i)
	{
	 switch(i)
	  {
	  case 1:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses0);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 2:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses1);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 3:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses2);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 4:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses3);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 5:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses4);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 6:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses5);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 7:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses6);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 8:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses7);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 9:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses8);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 10:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses9);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 11:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses10);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 12:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses11);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 13:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses12);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 14:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses13);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  case 15:{
		  MediaPlayer a1= MediaPlayer.create(getApplicationContext(),R.raw.ses14);
	    	a1.start();    
	        a1.setOnCompletionListener(new OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	        mp.release();

	    	    };
	    	});
		  break;
	  }
	  }
	}
    @Override
    public void onBackPressed() {
       return;
    }
	public void inrequestadd(final int dogru) {
		mInterstitial = new InterstitialAd(Oyun.this);
		//ca-app-pub-1312048647642571/5631917786 reklam
		mInterstitial.setAdUnitId("ca-app-pub-1312048647642571/6478955392");
		final	Intent  intent = new Intent(Oyun.this,Score.class);
		intent.putExtra("score", dogru);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mInterstitial.loadAd(new AdRequest.Builder().build());
				mInterstitial.setAdListener(new AdListener() {

					@Override
					public void onAdClosed() {
						super.onAdClosed();
						startActivity(intent);
					}

					@Override
					public void onAdFailedToLoad(int errorCode) {
						super.onAdFailedToLoad(errorCode);
						startActivity(intent);
					}

					@Override
					public void onAdLoaded() {
						super.onAdLoaded();
						if (mInterstitial.isLoaded()) {
							mInterstitial.show();
						}
						else {
							startActivity(intent);
						}
					}

				});

			}
		});

	}
}
